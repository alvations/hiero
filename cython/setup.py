from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

import os.path

setup(ext_modules = cythonize("svector.pyx", language="c++"))
setup(ext_modules = cythonize(Extension("cost", ["cost.pyx", "logp.c"]), language="c++"))
setup(ext_modules = cythonize("sym.pyx", language="c++"))
setup(ext_modules = cythonize(Extension("rule", ["rule.pyx", "strutil.c"]), language="c++"))

### from kenlm setup.py

import glob
import platform

kenlm_root = '/afs/crc.nd.edu/group/nlp/software/kenlm/devel'
FILES = (glob.glob(os.path.join(kenlm_root, 'util/*.cc')) + 
         glob.glob(os.path.join(kenlm_root, 'lm/*.cc')) +
         glob.glob(os.path.join(kenlm_root, 'util/double-conversion/*.cc')))
FILES = [fn for fn in FILES if not (fn.endswith('main.cc') or fn.endswith('test.cc'))]

LIBS = ['stdc++']
if platform.system() != 'Darwin':
    LIBS.append('rt')

ARGS = ['-O3', '-DNDEBUG', '-DKENLM_MAX_ORDER=6']

setup(ext_modules =
    cythonize(Extension('lm_kenlm',
                        sources=FILES + ['lm_kenlm.pyx'],
                        language='c++', 
                        include_dirs=[kenlm_root],
                        libraries=LIBS, 
                        extra_compile_args=ARGS))
)
