#!/bin/bash
#$ -q *@@nlp
#$ -pe mpi-* 16

module load ompi/1.8.7-gcc-4.4.7

NLP=/afs/crc.nd.edu/group/nlp
export HIERO=$NLP/01/dchiang/hiero

export PATH=$NLP/software/python/2.7.10/bin:$PATH
export PYTHONPATH=$HIERO:$HIERO/cython:$PYTHONPATH

INI=decoder.ini
CORPUS=dev
NAME=baseline3
PREFIX=$NLP/data/elisa/hau/y1r1.v2/tok/true/$CORPUS

fsync -d 60 trainer.$CORPUS.$NAME.{log,out,weights,scores} &

mpirun python $HIERO/trainer.py $INI -x $CORPUS -B IBM --heldout-sents 100 --heldout-policy uniform -W trainer.$CORPUS.$NAME.weights -L trainer.$CORPUS.$NAME.scores $PREFIX.{hau,eng} >trainer.$CORPUS.$NAME.out 2>trainer.$CORPUS.$NAME.log


