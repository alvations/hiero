#!/bin/bash
#$ -q *@@nlp
#$ -pe mpi-* 16

module load ompi/1.8.7-gcc-4.4.7

NLP=/afs/crc.nd.edu/group/nlp
export HIERO=$NLP/01/dchiang/hiero
MOSES=$NLP/software/moses/3.0/mosesdecoder

export PATH=$NLP/software/python/2.7.10/bin:$PATH
export PYTHONPATH=$HIERO:$HIERO/cython:$PYTHONPATH

SL=hau
TL=eng
INI=decoder.ini
CORPUS=test
NAME=baseline3
DATA=$NLP/data/elisa/$SL/y1r1.v2
WEIGHTS=trainer.dev.$NAME.weights.final

fsync -d 60 decoder.$CORPUS.$NAME.{log,out} &

mpirun python $HIERO/decoder.py $INI -p -x $CORPUS -w $WEIGHTS $DATA/tok/true/$CORPUS.$SL >decoder.$CORPUS.$NAME.out 2>decoder.$CORPUS.$NAME.log

$MOSES/scripts/tokenizer/detokenizer.perl <decoder.$CORPUS.$NAME.out >decoder.$CORPUS.$NAME.detok
$NLP/software/bleu/bleu.py decoder.$CORPUS.$NAME.detok $DATA/$CORPUS.$TL >>decoder.$CORPUS.$NAME.log
