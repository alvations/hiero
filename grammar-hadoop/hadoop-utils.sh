HADOOPTMP=${HADOOPTMP:-0}

hadoop_stream () {
    ARGS=
    MAPPER=
    REDUCER=
    INPUTS=
    OUTPUT=
    while [ $# -gt 0 ]; do
	if [ "$1" = "-mapper" ]; then
	    MAPPER=$2
	    shift 2
	elif [ "$1" = "-reducer" ]; then
	    REDUCER=$2
	    shift 2
	elif [ "$1" = "-input" ]; then
	    INPUTS="$INPUTS $2"
	    shift 2
	elif [ "$1" = "-output" ]; then
	    OUTPUT=$2
	    shift 2
	else
	    ARGS="$ARGS $1" # assume no spaces!
	    shift
	fi
    done

    if [ "x$INPUTS" = "x" ]; then
	if [ "x$HADOOPTMPS" = "x" ]; then
	    echo "Error: No input provided and no implicit input available" 1>&2
	    exit 1
	fi
	INPUTS="$HADOOPTMPS"
	HADOOPTRASH="$HADOOPTMPS"
	HADOOPTMPS=
    fi

    if [ "x$OUTPUT" = "x" ]; then
	OUTPUT=temp.$$.$HADOOPTMP
	HADOOPTMPS="$HADOOPTMPS $OUTPUT"
	HADOOPTMP=`expr $HADOOPTMP + 1`
    fi

    INPUTARGS=
    for INPUT in $INPUTS; do
	INPUTARGS="$INPUTARGS -input $INPUT"
    done

    echo "$HADOOPSTREAM $INPUTARGS -mapper \"${MAPPER:-/bin/cat}\" -reducer \"${REDUCER:-NONE}\" -output $OUTPUT $ARGS" 1>&2
    $HADOOPSTREAM $INPUTARGS -mapper "${MAPPER:-/bin/cat}" -reducer "${REDUCER:-NONE}" -output $OUTPUT $ARGS

    for FILE in $HADOOPTRASH; do 
	echo "hadoop fs -rmr $FILE" 1>&2
	hadoop fs -rmr $FILE
    done
}
