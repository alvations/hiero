#!/bin/sh

# Usage: filter.sh <filter-file> <output-dir>
# Assumes that a Hadoop cluster exists and HADOOP_CONF_DIR is set
# This doesn't necessarily have to be run on a node inside the cluster

# These are paths internal to Notre Dame
NLP=/afs/crc.nd.edu/group/nlp

module load java/1.7

module load python/2.7.8
PYTHON=`which python`

HIERO=$NLP/01/dchiang/hiero
export PATH=$HIERO:$HIERO/grammar-hadoop:$PATH

export HADOOP_PREFIX=$NLP/software/hadoop/1.2.1
export PATH=$HADOOP_PREFIX/bin:$PATH
. $HIERO/grammar-hadoop/hadoop-utils.sh
HADOOPSTREAM="hadoop jar $HADOOP_PREFIX/contrib/streaming/hadoop-streaming-*.jar -cmdenv PYTHONPATH=$HIERO:$HIERO/grammar-hadoop:$HIERO/cython -cmdenv LD_LIBRARY_PATH=$HIERO/cython"

WORKDIR=.
hadoop fs -mkdir $WORKDIR

NODES=`wc -l < $HADOOP_CONF_DIR/slaves`

# A URL (file: or hdfs:) pointing to a file or directory of unfiltered rules
RULES=file://$(pwd)/rules.final

# A file of sentences, one per line, to build grammars for
INFILE=$1

# Directory to put grammars into
OUTDIR=$(cd $(dirname "$2") && pwd -P)/$(basename "$2")
rm -rf $OUTDIR
mkdir -p $OUTDIR

echo Building per-sentence grammars for $INFILE

# remove dummy output directory that we are about to create
hadoop fs -rmr null

##################################################################
# Map:    read in a rule and write out (segid, rule) for each segid
#         that rule matches
# Reduce: (segid, rule) -> write rule to sentence grammar #segid

# Turn off speculative execution for the reducer because it
# outputs to directories, not to stdout
$HADOOPSTREAM \
    -input $RULES \
    -output null \
    -mapper "$PYTHON $HIERO/grammar-hadoop/filter.py -l6 $INFILE" \
    -reducer "$PYTHON $HIERO/grammar-hadoop/sentgrammars.py $OUTDIR" \
    -jobconf mapred.reduce.tasks.speculative.execution=false
