Grammar-building scripts for a single CPU
=========================================

This directory contains the old scripts for building a grammar on a
single CPU. These were replaced by the scripts in `grammar-hadoop`,
which uses Hadoop.

Before the training proper, you must obtain (1) a word-aligned bitext
and (2) word-translation probabilities. These can be obtained from the
Pharaoh trainer or by using refiner.py. To do the latter, you need to
run GIZA++ on the training bitext to produce Viterbi word alignments
in both directions. Then run:
	
	refiner.py f-e.A3.final e-f.A3.final > refined
	lexweights.py refined -w lex.n2f lex.f2n

where f-e.A3.final is the Viterbi alignment for P(f|e), i.e., it
aligns multiple French words to an English word, and e-f.A3.final is
the same but in the reverse direction. The refiner outputs another
GIZA++-style word alignment on standard output, and lexweights.py
outputs two tables of word-translation probabilities: lex.n2f is
P(f|e) and lex.f2n is P(e|f).

Alternative usage: whenever a GIZA++-style alignment file is expected,
you can specify the -P option and then supply alignments on stdin in
the form
	0-1 1-2 2-3
meaning French word 0 is aligned with English word 1, etc. In this
case you must specify the -W <french-file> <english-file> option to
supply the raw parallel text.

Step 1: extract rules

	extractor.py <option>* [<input-file>] -o <output-dir>

If <input-file> is omitted, standard input is used. The input format
is a GIZA++-style word alignment (the output of refiner.py). Or, it
can be word alignments in the sentence-per-line format described
above, using the same additional options.


The extractor will output multiple "pre-grammar" files into
<output-dir>. Note that if <output-dir> is not already empty, the
result could be a mess.

Other options (partial list):
	-L <len>	maximum initial phrase length
	-l <len>	maximum rule length
	-t		"tight" phrases: no unaligned words at edges
	-s <len>	minimum subphrase length
	-w lex.n2f lex.f2n word-translation probability tables
	-A		preserve word alignments

Step 2: generate final grammar

	scorer.py <option>* <input>*

The <input>s are either files or directories. If a directory, it will
read all the files in the directory. The grammar is written to
standard output.

Options (partial list):
	-l <len>	maximum rule length
	-f <file>	specify a test file to filter grammar for
